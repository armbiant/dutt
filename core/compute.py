from datetime import datetime, timedelta
from dateutil import tz
from retrying import retry
from typing import List
from context import db
from model import HELLO, OneDay


def _now() -> str:
    return str(int(datetime.now().timestamp()))


def _today() -> OneDay:
    today = datetime.utcnow().date()
    start = datetime(today.year, today.month, today.day, tzinfo=tz.tzutc())
    end = start + timedelta(1)
    return OneDay(start=str(int(start.timestamp())), end=str(int(end.timestamp())))


@retry(stop_max_attempt_number=3, stop_max_delay=3000, wait_fixed=1000)
def _fetch_all_day(day: OneDay, filter: dict = {}) -> List[dict]:
    filter["key?r"] = [day.start, day.end]
    res = db.fetch(query=filter)
    if res.count == 0:
        return []
    all_items = res.items
    while res.last:
        res = db.fetch(last=res.last)
        all_items += res.items
    return all_items


def _has_hello() -> bool:
    today: OneDay = _today()
    data = db.fetch(query={"key?r": [today.start, today.end], "task": HELLO})
    if data.count > 0:
        return True
    return False
