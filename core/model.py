from pydantic import BaseModel
from typing import Union, List

HELLO: str = "Hello"


class TaskRequest(BaseModel):
    key: str
    project: Union[str, None] = None
    task: str
    is_a_break: bool = False
    is_ignored: bool = False


class TaskResponse(BaseModel):
    key: str
    project: Union[str, None] = None
    task: str
    is_a_break: bool = False
    is_ignored: bool = False
    duration: int
    date_: str
    time_: str


class TasksResponse(BaseModel):
    items: List[TaskResponse] = []


class Task(BaseModel):
    key: str
    project: Union[str, None] = None
    task: str
    is_hello: bool = False
    is_a_break: bool = False
    is_ignored: bool = False


class OneDay(BaseModel):
    start: str
    end: str
