#!/bin/bash
export PATH="/workspace/.detaspace/bin:$PATH"
if [ -v SPACE_TOKEN ]; then
    echo "==> Set Space Token."
    mkdir /home/gitpod/.detaspace/
    echo "{\"access_token\":\"${SPACE_TOKEN}\"}" > /home/gitpod/.detaspace/space_tokens
    chmod 600 /home/gitpod/.detaspace/space_tokens
else
    echo "==> Please login to Space or set SPACE_TOKEN before running commands."
fi
if [ ! -f Spacefile ]; then
    echo "==> New Space project, start new."
    space new
else
    echo "==> Space already initialised"
    if [ -v SPACE_PROJECT_ID ]; then
        echo "==> Linking workspace to project $SPACE_PROJECT_ID"
        space link -i $SPACE_PROJECT_ID
    else
        echo "==> No Space Project ID found (SPACE_PROJECT_ID), manual link needed."
    fi
fi
