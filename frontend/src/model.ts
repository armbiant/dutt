export type TaskRequest = {
  key?: string
  project?: string
  task?: string
  is_a_break: boolean
  is_ignored: boolean
}

export type TaskResponse = {
  key: string
  project?: string
  task: string
  is_a_break: boolean
  is_ignored: boolean
  duration: number
  time_: string
  date_: string
}

export type TasksResponse = {
  items: Array<TaskResponse>
}
