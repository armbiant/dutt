from starlette.config import Config
import logging
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from deta import Deta

config = Config(".env")
DETA_PROJECT_KEY = config("DETA_PROJECT_KEY", default=None)
CORS = config("CORS", cast=bool, default=None)

deta = Deta(DETA_PROJECT_KEY)
db = deta.Base("track")


def cors(app: FastAPI):
    if CORS:
        logging.info("CORS * enabled")
        app.add_middleware(
            CORSMiddleware,
            allow_origins=["*"],
            allow_credentials=True,
            allow_methods=["*"],
            allow_headers=["*"],
        )
