import os
import logging
from datetime import datetime
from fastapi import FastAPI, HTTPException, status
import humanfriendly as hf
from model import HELLO, OneDay, Task, TasksResponse, TaskRequest, TaskResponse
from context import cors, db
from compute import _has_hello, _now, _today, _fetch_all_day

app = FastAPI()
cors(app)


@app.get("/version")
def version():
    return {"version": os.getenv("VERSION")}


@app.put("/hello")
def hello():
    if not _has_hello():
        db.put(Task(key=_now(), task=HELLO, is_hello=True).dict())
    else:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT)


@app.get("/hello")
def has_hello():
    if _has_hello():
        return
    else:
        raise HTTPException(status_code=status.HTTP_204_NO_CONTENT)


@app.post("/task")
def post_task(task: TaskRequest):
    db.put(Task(key=_now(), **task.dict()).dict())


@app.get("/tasks/today")
def get_tasks() -> TasksResponse:
    try:
        response: TasksResponse = TasksResponse()
        today: OneDay = _today()
        all_items = _fetch_all_day(today)
        # TODO sort by key asc
        prevDatetime = None
        for item in all_items:
            taskDatetime = datetime.fromtimestamp(int(item["key"]))
            # Compute duration
            duration = 0
            if prevDatetime:
                duration = (taskDatetime - prevDatetime).seconds
            prevDatetime = taskDatetime
            # TODO Compute time
            taskTime = taskDatetime.strftime("%H:%M:%S")
            # TODO Compute date
            taskDate = taskDatetime.strftime("%Y-%m-%d")

            resp = TaskResponse(**item, duration=duration, date_=taskDate, time_=taskTime)
            response.items.append(resp)
        return response
    except Exception as ex:
        logging.exception(ex)
        raise ex


@app.get("/report")
def report(from_date: str = None, to_date: str = None):
    result = db.fetch()
    response = list()
    ts1 = None
    for row in result.items:
        data: Task = Task(**row)
        ts = datetime.datetime.fromtimestamp(int(data.key))
        if row["is_hello"]:
            ts1 = ts
        else:
            if not data.is_ignored:
                delta = ts - ts1
                rep = data.dict()
                rep["ts"] = ts.strftime("%Y-%m-%d %H:%M:%S")
                rep["delta_str"] = hf.format_timespan(delta, max_units=2)
                rep["delta_sec"] = delta.seconds
                del rep["key"]
                del rep["is_hello"]
                del rep["is_a_break"]
                del rep["is_ignored"]
                response.append(rep)
            ts1 = ts
    return response
