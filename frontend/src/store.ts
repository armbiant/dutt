import { writable } from 'svelte/store'

export const refreshToday = writable(false)
